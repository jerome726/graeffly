<?php
include_once("./include/class/class_bddmanager.php");
include_once("./include/class/class_specialdate.php");

class Stat {
    var $bdd;
	var $specialDate;
	var $dateStat;
	var $hourStat;
	var $ipStat;
	var $refererStat;
	var $errorStat;
	
	function Stat() {
        $this->bdd = new bddManager();
		$this->specialDate = new Specialdate();
		$this->dateStat = "";
		$this->hourStat = "";
		$this->ipStat = "";
		$this->refererStat = "";
		$this->errorStat = false;
	}
	
	function getDateStat() {
		return $this->dateStat;
	}
	
	function getHourStat() {
		return $this->hourStat;
	}
	
	function getIpStat() {
		return $this->ipStat;
	}
	
	function getReferer() {
		return $this->refererStat;
	}
	
	function getErrorStat() {
		return $this->errorStat;
	}
		
	function insertBddStat() {
		$this->dateStat = $this->specialDate->getDateToday();
		$this->hourStat = $this->specialDate->getHourNow();
		$this->ipStat = $_SERVER["REMOTE_ADDR"];
		
		if(isset($_SERVER["HTTP_REFERER"])) {
			$this->refererStat = $_SERVER["HTTP_REFERER"];
		}
		
		$sql = "INSERT INTO stat (
					date_stat,
					hour_stat,
					ip_stat,
					referer_stat
				) VALUES (
					'$this->dateStat',
					'$this->hourStat',
					'$this->ipStat',
					'$this->refererStat'
				)";

		// 1 true, 0 false
		$this->errorStat = $this->bdd->executeQueryBdd($sql);
	}
}    
?>
