<?php
class SpecialDate {

	var $dateToday;
	var $dateTodayPretty;
	var $hourNow;
	
	function SpecialDate() {
		$this->dateToday = "";
		$this->dateTodayPretty = "";
		$this->hourNow = "";
	}
	
	function getDateToday() {
		$this->dateToday = date("Y-m-d");
		
		return $this->dateToday;
	}
	
	function getDateTodayPretty($date) {
		list($aa, $mm, $jj) = explode("-", $date);
		$this->dateTodayPretty = "$jj/$mm/$aa";
		
		return $this->dateTodayPretty;
	}
	
	function getHourNow() {
		$this->hourNow = date("H:i");
		
		return $this->hourNow;
	}
}    
?>
