<?php
class BddManager
{
	var $queryBdd;
	var $resultBdd;
	
	function BddManager() {
		$this->queryBdd = "";
		$this->resultBdd = NULL;
	}
	
	function getQueryBdd() {
		return $this->queryBdd;
	}	
		
	function executeQueryBdd($x) {
		$this->queryBdd = $x;
				
		if(!$this->resultBdd = @mysql_query($this->queryBdd)) {
			return false;
		} else {
			return true;
		}
	}
	
    function fetchArrayBdd() {
        return @mysql_fetch_array($this->resultBdd);
    } 
	
    function getNbrLineBdd() {
        return @mysql_num_rows($this->resultBdd);
    } 
}
?>
