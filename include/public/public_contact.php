<div id="contact">
	<div class="titre_paragraphe">
		UN AVOCAT PARISIEN
	</div>
	
	Le Cabinet de Ma�tre Romain GRA�FFLY est situ� au rez-de-chauss�e du 23, rue B�nard dans le quatorzi�me arrondissement, entre les quartiers Al�sia et Pernety, non loin de la porte d�Orl�ans et de la Gare Montparnasse.
	<br/><br/>
	En plein c�ur du quartier Plaisance, la rue B�nard commence au 22, rue des Plantes et se termine au 49, rue de la Sabli�re.
	<br/><br/>
	La rue B�nard donne dans la rue Pernety, qui est est desservie par la ligne de M�tro parisien n� 13 (Asni�res - Gennevilliers - Les Courtilles - Saint-Denis - Universit� / Ch�tillon - Montrouge), � la station Pernety.
	<br/><br/>
	La rue B�nard est �galement proche de la station de M�tro Al�sia, sur la ligne 4 (Porte de Clignancourt - Montrouge).


	<div class="titre_paragraphe">
		COORDONNEES
	</div>

	<div id="photo_contact">
		<a href="./image/romain2.jpg" class="image_zoom"><img src="./image/romain2_thumb.jpg" alt="<?php echo $ALT; ?>" title="<?php echo $ALT; ?>"></a>
	</div>
	
	Ma�tre Romain GRA�FFLY<br/>
	23, rue B�nard<br/>
	75014 PARIS<br/>
	<br/>
	<i>Tel.&nbsp;:</i>&nbsp;01&nbsp;73&nbsp;70&nbsp;48&nbsp;06<br/>
	<br/>
	<i>Fax.&nbsp;:</i>&nbsp;01&nbsp;40&nbsp;43&nbsp;95&nbsp;88
	<br/>
	<br/>
	<i>Adresse : </i>romain<img src="./image/arobase.jpg" class="arobase" />graeffly.com
	<br/><br/>
	<span class="important">Uniquement sur rendez-vous.</span>
	<br/><br/><br/><br/>
	<i>Administrateur du site : </i>webmaster<img src="./image/arobase.jpg" class="arobase" />graeffly.com
	<br/>
	
</div>
