<div id="accueil">
	<div class="titre_debut_texte">
		Un avocat parisien, � votre service.
	</div>
	
	<div id="photo_romain_3">
		<a href="./image/romain3.jpg" class="image_zoom"><img src="./image/romain3_thumb.jpg" alt="<?php echo $ALT; ?>" title="<?php echo $ALT; ?>"></a>
	</div>
	
	Le Cabinet GRA�FFLY a �t� cr�� en ao�t 2008 par Romain GRA�FFLY, Docteur en Droit, et ancien 
	attach� d�enseignement et de recherches � l�Universit� Panth�on-Assas (Paris II).
	<br/><br/>
	Romain GRA�FFLY est l�auteur de nombreux ouvrages et articles de r�f�rence en sciences politiques et en droit, notamment en droit du logement social.
	<br/><br/>
	Install� depuis janvier 2015 au c�ur du quartier Plaisance, et travaillant au sein d�une structure pluridisciplinaire et dynamique, il intervient dans plusieurs secteurs d�activit�s � destination des particuliers, des petites et moyennes entreprises, mais aussi des associations, des comit�s d�entreprises, ainsi que des syndicats et des collectivit�s publiques.
	<br/><br/>
	Nombre de dossiers sont travaill�s en r�seau avec les autres membres de son Cabinet, de telle sorte que chaque Client est en mesure d�obtenir une r�ponse appropri�e et rapide � la question pos�e.
	<br/>
	
	<div id="photo_romain_4">
		<a href="./image/romain4.jpg" class="image_zoom"><img src="./image/romain4_thumb.jpg" alt="<?php echo $ALT; ?>" title="<?php echo $ALT; ?>"></a>
	</div>
	
	<br/>
	Romain GRA�FFLY a pour principe de r�pondre dans les meilleurs d�lais � un appel t�l�phonique, un courriel, un courrier ou une t�l�copie. La disponibilit� est une r�gle qu�il a plac�e � �galit� avec les seize principes fondamentaux de la profession d�Avocat.
	<br/><br/>
	Le Cabinet GRA�FFLY adapte ses tarifs � la situation des particuliers ou des entreprises, en respect des r�gles et des usages de son M�tier.
	<br/><br/>
	Il pratique soit un tarif forfaitaire, soit un tarif horaire, en fonction des prestations. 
	<br/><br/>
	Les modalit�s d�intervention et de r�mun�ration sont d�termin�es d�s le premier rendez-vous avec le Client.
	<br/><br/>
	A ce titre et enfin, on signalera que ce premier rendez-vous est toujours factur� au Client, � savoir la somme de 100,00 euros.
	<br/><br/>	
	
	<div class="centrer">
		<span class="important">Contact&nbsp;:&nbsp;01&nbsp;73&nbsp;70&nbsp;48&nbsp;06</span> 
	</div>
</div>