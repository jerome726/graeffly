<?php
$TITLE = "Romain GRAEFFLY";
$ALT = "Romain Gra�ffly avocat";

$KEYWORDS = "graeffly, gra�ffly, romain, avocat, avocats, paris, barreau de paris, cagliostro, H�tel de Cagliostro";
$DESCRIPTION = "Romain GRAEFFLY est avocat au Barreau de Paris, dont les champs de comp�tences sont le droit administratif, le droit du logement et du logement social, et le droit de la fonction publique. Il intervient �galement en tant que g�n�raliste. Ses clients sont divers (particuliers, associations, soci�t�s priv�es et personnes publiques).";
?>
