<div id="fonctions">
	<div class="rond">
		<span class="important">Agent de sportifs</span> : sur le fondement de la loi n� 2011-331 du 28 mars 2011 de modernisation des professions judiciaires ou juridiques, l�avocat peut d�sormais repr�senter, en qualit� de mandataire le sportif dans sa carri�re professionnelle.
	</div>
	<div class="rond">
		<span class="important">Membre de la Commission Juridique et de Discipline de la Ligue nationale de Basket Ball (2009-2017)</span>
	</div>
	<div class="rond">
		<span class="important">Collaborateur � France Action Locale / Groupe DEMOS</span> (75008) : organisation et animation de s�minaires sur le droit et les institutions � destination des �lus locaux et des agents publics.
	</div>
	<div class="rond">
		<span class="important">Collaborateur au Jurisclasseur</span> (75015) : auteur du fascicule relatif au Droit au logement.
	</div>
</div>
