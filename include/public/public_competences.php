<div id="competences">

	<div class="titre_paragraphe">
		PRINCIPALES MATIERES PRATIQUEES
	</div>
	<div class="fleche">
		Droit administratif.
	</div>
	<div class="fleche">
		Droit du logement et du logement social.
	</div>
	<div class="fleche">
		Droit de la fonction publique.
	</div>
	<div class="fleche">
		Droit du sport.
	</div>

	<div class="titre_paragraphe">
		FORMATION
	</div>
	<div class="rond">
		2006-2007 : <span class="important">El�ve de l�Ecole de Formation professionnelle des Barreaux du ressort 
		de la Cour d�appel de Paris (EFB)</span>.
	</div>
	<div class="rond">
		2005 : <span class="important">Prix de th�se de l�Universit� Panth�on-Assas (Paris II)</span> - Section Droit public.
	</div>
	<div class="rond">
		2004 : <span class="important">Docteur en Droit mention Droit public,</span> Universit� Panth�on-Assas (Paris II). 
		R�alisation d�une th�se intitul�e : � Le logement social en France et en Europe occidentale : �tude comparative de 
		l�intervention publique � sous la direction du professeur Jacques CHEVALLIER. Mention tr�s honorable avec les 
		f�licitations du jury � l�unanimit� assortie d�une autorisation de publication imm�diate, d�une pr�sentation � un 
		prix de th�se et pour l�octroi d�une subvention en vue de publication.
	</div>
	<div class="rond">
		1999 : <span class="important">Major du DEA de Sciences Administratives,</span> Universit� Panth�on-Assas (Paris II). 
		R�alisation d�un m�moire sous la direction du professeur Jacques CHEVALLIER intitul� � Les relations entre les Offices 
		HLM et leurs usagers �. Attribution d�une allocation de recherche de trois ans. Rattachement au Centre d��tudes et de 
		recherches de Sciences Administratives (CERSA), unit� associ�e au Centre national de la recherche sup�rieure (UMR 7106).
	</div>
	<div class="rond">
		1998 : <span class="important">Ma�trise en Droit Public,</span> Universit� Panth�on-Assas (Paris II). R�alisation d�un 
		m�moire sous la direction du professeur Jean-Claude VENEZIA intitul� � Le service public du logement social �.
	</div>
	
</div>