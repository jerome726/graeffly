<div id="publications">

	<div class="titre_paragraphe">
		OUVRAGES
	</div>
	<div class="rond">
		Logement social et politiques de non-discrimination en Europe, L�harmattan, collection Questions contemporaines, Paris, f�vrier 2008, 221 pp.
	</div>
	<div class="rond">
		Le logement social : �tude compar�e de l�intervention publique en France et en Europe occidentale, Librairie g�n�rale de droit et de jurisprudence, collection Biblioth�que de droit public, Tome 247, Paris, novembre 2006, 626 pp.
	</div>
	<div class="rond">
		Sous la direction d�Yves J�gouzo, Droit de l�am�nagement, Le Moniteur, collection R�f�rences, novembre 2006, 1860 pp. 
	</div>
	
	
	<div class="titre_paragraphe">
		ARTICLES
	</div>
	<div class="rond">
		� R�flexions sur les �volutions du logement social en Europe �, Bulletin d�information de la Soci�t� du logement de la R�gion bruxelloise, n� 58, juin 2009, pp. 15-21.
	</div>
	<div class="rond">
		� La mise en �uvre du droit au logement opposable : premier bilan �, Revue de Droit Sanitaire et Social, n� 4/2009, juillet-ao�t 2009, pp. 741-751.
	</div>
	<div class="rond">
		� Le gardien d�HLM � travers la jurisprudence administrative : de l�arr�t Lauthier � nos jours �, Actualit� juridique fonctions publiques, n� 3, mai-juin 2009, pp. 161-167.
	</div>
	<div class="rond">
		� L�office public de l�habitat : un nouveau statut pour des nouvelles politiques publiques de l�habitat �, Le conseiller g�n�ral, n� 26, f�vrier-mars 2009, pp. 129-136.
	</div>
	<div class="rond">
		� Concession publique d�am�nagement : le d�clin progressif de la conception fran�aise �, Le conseiller g�n�ral, n� 25, d�cembre 2008-janvier 2009, pp. 69-72.
	</div>
	<div class="rond">
		� L�histoire du logement social en Finlande : du droit au logement aux aides ARAVA �, Le conseiller g�n�ral, n� 22, avril-mai 2008, pp. 153-160.
	</div>
	<div class="rond">
		� Le logement en Europe : les politiques anti-discriminatoires �, Etudes fonci�res, n� 132, mars-avril 2008, pp. 7-11.
	</div>
	<div class="rond">
		� L�histoire du logement social en Allemagne �, Le conseiller g�n�ral, n� 19, septembre-octobre 2007, pp. 125-134.
	</div>
	<div class="rond">
		� La r�forme du logement municipal britannique (1975-2007) �, Etudes fonci�res, n� 129, septembre-octobre 2007, pp. 14-19.
	</div>
	<div class="rond">
		� Aspects administratifs de la loi n� 2007-290 du 5 mars 2007 instituant le droit au logement opposable et portant diverses mesures en faveur de la coh�sion sociale �, Droit administratif, juillet 2007, n� 7, pp. 11-15.
	</div>
	<div class="rond">
		� L�histoire du logement social en Belgique �, Le conseiller g�n�ral, n� 17, mars-avril 2007, pp. 121-130.
	</div>
	<div class="rond">
		� Les modes de gestion du logement social en Europe �, Le conseiller g�n�ral, n� 16, janvier-f�vrier 2007, pp. 165-170.
	</div>
	<div class="rond">
		� Qualifications du logement social adopt�es par les pays europ�ens �, Le conseiller g�n�ral, n� 15, d�cembre 2006, pp. 103-108.
	</div>
	<div class="rond">
		� Les implications du droit au logement �, Le conseiller g�n�ral, n� 14, septembre 2006, pp. 78-80.
	</div>
	<div class="rond">
		� Les politiques sociales du logement en Norv�ge �, Etudes fonci�res, n� 122, juillet-ao�t 2006, pp. 24-27.
	</div>
	<div class="rond">
		� Logement social et Etats-providence europ�ens �, Le conseiller g�n�ral, n� 13, juin 2006, pp. 34-38.
	</div>
	<div class="rond">
		� Panorama d�une pr�rogative fonci�re locale : le droit de l�expropriation dans l�Union europ�enne �, Etudes fonci�res, n� 121, mai-juin 2006, pp. 24-29.
	</div>
	<div class="rond">
		� Le logement social en Europe occidentale �, Bulletin d�information de la Soci�t� du logement de la R�gion bruxelloise, n� 46, avril-mai 2006, pp. 8-18.
	</div>
	<div class="rond">
		� Le Conseil constitutionnel alg�rien : de la greffe institutionnelle � l�av�nement d�un contentieux constitutionnel ? �, Revue du droit public, n� 5-2005, pp. 1381-1404.
	</div>
	<div class="rond">
		� Vers une unification des politiques publiques de lutte contre les discriminations �, Actualit� juridique droit administratif, n� 17, 2 mai 2005, pp. 934-941.
	</div>
	
	
	<div class="titre_paragraphe">
		RAPPORTS OFFICIELS
	</div>
	<div class="rond">
		Logement social et politiques de non-discrimination en Europe, Rapport public sous l��gide de l�agence nationale pour la coh�sion sociale et l'�galit� des chances (ACSE), 2007, 175 pp.
	</div>


	<div class="titre_paragraphe">
		NOTES D�ARRETS
	</div>
	<div class="rond">
		Ccass., 3e civ., 3 mai 2007, � Marraud des Grottes (M.) c/ Pr�fet de r�gion Martinique �, Actualit� juridique droit immobilier, n�1, janvier 2008, pp. 48-50.
	</div>
	<div class="rond">
		CE, 23 juin 2006, � Soci�t� Actilor �, Actualit� juridique droit administratif, n� 36, 30 octobre 2006, pp. 2026-2029.
	</div>
	<div class="rond">
		Ccass., 3e civ., 13 juillet 2005, � Chatonville (st�) c/ de Cuverville �, Actualit� juridique droit immobilier, n� 11, 10 novembre 2005, pp. 847-848.
	</div>
	<div class="rond">
		Ccass., 3e civ., 13 juillet 2005, � Ets R�seau ferr� de France c/ St� Laser propret� �, Actualit� juridique droit immobilier, n� 10, 10 octobre 2005, pp. 758-760.
	</div>
	<div class="rond">
		Ccass., 3e civ., 12 juillet 2005, � Chapard (Mme) c/ Hazane (Mme) et autres �, Actualit� juridique droit immobilier, n� 10, 10 octobre 2005, pp. 750-751.
	</div>
	
	
	<div class="titre_paragraphe">
		INTERVENTIONS ET NOTES
	</div>
	<div class="rond">
		� La loi du 30 d�cembre 2004 portant cr�ation de la Haute autorit� de lutte contre les discriminations et pour l��galit� � publi� sous le titre � Analyse des �volutions aux plans national et territorial �, Actes des assises nationales du Fonds d�action et de soutien pour l�int�gration et la lutte contre les discriminations, Maison de la Mutualit� � Paris, 26 septembre 2005, pp. 22-24 et � Les r�ponses de l�action publique : de nouveaux dispositifs institutionnels �, Lettre du Fasild, n� 65, mars-avril 2006, p. 13.
	</div>
	<div class="rond">
		� Logement social et Etats-providence europ�ens �, GIS sociologie de l�Habitat (Universit� Paris XII), Journ�e d��tude sur le logement et l�habitat comme objet de recherche, Cr�teil, 20 mai 2005, 10 pp.
	</div>
	<div class="rond">
		En collaboration avec Jean-Philippe Brouant et Paule Quilichini, Politique du logement et d�centralisation : commande du minist�re de l�Int�rieur (Centre d�Etudes et de Pr�vision) � GRIDAUH, f�vrier 2003, 31 pp.
	</div>
	<div class="rond">
		Rapports sur le logement social en Grande-Bretagne, Belgique et Allemagne, GRIDAUH, janvier 2001, 247, 115 et 55 pp.
	</div>
	<div class="rond">
		� Villes et territoires : logiques publiques et logiques de march� �, GRIDAUH, octobre 2000, 9 pp.
	</div>
	
	
	<div class="titre_paragraphe">
		PRESSE
	</div>
	<div class="rond">
		� Acc�s au logement : comment aider les salari�s ? �, Social CE, hors s�rie sp�cial Bons plans 2009, avril 2009, pp. 22-23.
	</div>
	<div class="rond">
		� Plus d��galit� dans l�habitat social, une ambition europ�enne �, Pouvoirs locaux, n� 79, IV, 2008, pp. 133-134.
	</div>
	<div class="rond">
		� HLM : interventions � tous les �tages �, Marianne, 22 au 28 septembre 2007.
	</div>
	<div class="rond">
		� Le logement social est-il de gauche ? �, Le Parisien, samedi 8 septembre 2007.
	</div>
	<div class="rond">
		� Logement social, les trois mod�les europ�ens �, Le Monde, vendredi 23 f�vrier 2007.
	</div>
	<div class="rond">
		� Trois questions �� Romain Gra�ffly �, La voix du Nord, jeudi 11 janvier 2007.
	</div>
	
</div>