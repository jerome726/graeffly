<div id="presse">
	<div class="rond">
		Social CE, Hors s�rie 2009
		<div class="photo_presse">
			<a href="./image/presse/social_ce_2009.jpg" rel="diaporama_group" title="Romain Graeffly avocat - Social CE, Hors s�rie 2009">
				<img width="500" height="700" src="./image/presse/social_ce_2009.jpg" alt="romain graeffly avocat Social CE, Hors s�rie 2009" title="Social CE, Hors s�rie 2009" />
			</a>
		</div>
	</div>
	<br/>
	<div class="rond">
		Marianne, 22 au 28 septembre 2007
		<div class="photo_presse">
			<a href="./image/presse/marianne_2007.jpg" rel="diaporama_group" title="Romain Graeffly avocat - Marianne 2007">
				<img width="500" height="500" src="./image/presse/marianne_2007.jpg" alt="romain graeffly avocat Marianne 2007" title="Marianne 2007" />
			</a>
		</div>
	</div>
	<br/>
	<div class="rond">
		La Voix du Nord, 11 janvier 2007
		<div class="photo_presse">
			<a href="./image/presse/la_voix_du_nord_2007.jpg" rel="diaporama_group" title="Romain Graeffly avocat - La Voix du Nord 2007">
				<img width="500" height="230" src="./image/presse/la_voix_du_nord_2007.jpg" alt="romain graeffly avocat La Voix du Nord 2007" title="La Voix du Nord 2007" />
			</a>
		</div>
	</div>
	<br/>
	<div class="rond">
		Le Parisien, 8 septembre 2006
		<div class="photo_presse">
			<a href="./image/presse/le_parisien_2006.jpg" rel="diaporama_group" title="Romain Graeffly avocat - le Parisien 2006">
				<img width="500" height="500" src="./image/presse/le_parisien_2006.jpg" alt="romain graeffly avocat le Parisien 2006" title="le Parisien 2006" />
			</a>
		</div>
	</div>
	<br/>
	<div class="rond">
		La lettre du FASILD, mars-avril 2006
		<div class="photo_presse">
			<a href="./image/presse/la_lettre_du_fasild_2006.jpg" rel="diaporama_group" title="Romain Graeffly avocat - La lettre du FASILD 2006">
				<img width="500" height="650" class="last" src="./image/presse/la_lettre_du_fasild_2006.jpg" alt="romain graeffly avocat La lettre du FASILD 2006" title="La lettre du FASILD 2006" />
			</a>
		</div>
	</div>
</div>