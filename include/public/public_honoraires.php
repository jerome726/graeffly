<div id="honoraires">
	Il est souvent difficile de savoir, au tout d�but d'une affaire, quel en sera le co�t parce que tous les �l�ments 
	ne sont pas connus, mais il est important d'aborder la question des honoraires d�s le premier rendez-vous.
	<br/><br/>
	<span class="important">Romain GRA�FFLY �tablit ses honoraires en respect de l�article 11 du R�glement Int�rieur 
	National de la profession d�Avocat (RIN)</span>, lequel dispose que la r�mun�ration de l�avocat est fonction de 
	chacun des �l�ments suivants conform�ment aux usages : 

	<div class="rond">
		le temps consacr� � l�affaire, 
	</div>
	<div class="rond">
		le travail de recherche, 
	</div>
	<div class="rond">
		la nature et la difficult� de l�affaire, 
	</div>
	<div class="rond">
		l�importance des int�r�ts en cause, 
	</div>
	<div class="rond">
		l�incidence des frais et charges du cabinet auquel il appartient, 
	</div>
	<div class="rond">
		sa notori�t�, ses titres, son anciennet�, son exp�rience et la sp�cialisation dont il est titulaire, 
	</div>
	<div class="rond">
		les avantages et le r�sultat obtenus au profit du client par son travail, ainsi que le service rendu � celui-ci, 
	</div>
	<div class="rond">
		la situation de fortune du client. 
	</div>

	<br/>
	<div class="titre_debut_texte">
		D�s lors, quatre formules peuvent �tre envisag�es :
	</div>

	<div class="titre_paragraphe">
		HONORAIRES FORFAITAIRES
	</div>
	Une r�mun�ration globale est d�termin�e d�s le premier entretien. En pratique, cette formule tr�s pratique est 
	notamment utilis�e pour les proc�dures simples et soumises � peu d'al�as. Le forfait ne comprendra pas les frais 
	de proc�dure (huissiers, experts, mandataires, confr�res postulants, avou�s et Avocats aux Conseils). Une provision 
	sup�rieure � 50 % du total est demand�e � l�ouverture du dossier.

	<div class="titre_paragraphe">
		HONORAIRES AU TEMPS PASSE
	</div>
	A l�ouverture du dossier est fix�e une r�mun�ration horaire adapt�e. Le taux horaire retenu ne comprendra pas non 
	plus les frais de proc�dure. Il varie en pratique selon la dur�e et les difficult�s de la proc�dure et il est 
	fix�, en commun accord, entre les parties. Une provision est demand�e � l�ouverture du dossier.
	
	<div class="titre_paragraphe">
		HONORAIRES DE RESULTAT
	</div>
	Le Droit fran�ais interdit la convention dite de � Quota Litis �, par laquelle un Avocat et son client conviendraient 
	que les honoraires ne seraient dus qu'en cas de succ�s et en fonction du r�sultat obtenu. Il est en revanche possible 
	de convenir, par �crit, d'un honoraire minimum forfaitaire et d'un honoraire compl�mentaire de r�sultat en pourcentage 
	du r�sultat obtenu. Une provision est �galement demand�e � l�ouverture du dossier.

	<div class="titre_paragraphe">
		L�ABONNEMENT
	</div>
	Certaines prestations permettent au client de b�n�ficier, moyennant un honoraire forfaitairement fix�, des services de 
	son Conseil pendant une dur�e convenue. Cette formule est particuli�rement adapt�e aux personnes morales 
	(soci�t�s, syndicats, associations, etc�) souhaitant une assistance en continu.

</div>