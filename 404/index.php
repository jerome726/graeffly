<?php
include("../include/public/conf/include_404.php");
?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>
		<?php
		echo $TITLE." - Erreur 404";
		?>
	</title>
	
	<link rel="shortcut icon" href="../image/icon.ico">
	<link rel="stylesheet" href="../css/index.css">
	<script type="text/javascript" src="../js/main.js">
	</script>
</head>

<body onload="ShowStatus()">
	<div id="error_404">
		<a href="http://www.graeffly.com/start.php?num_page=1">
			<img src="../image/404.jpg" id="img_error_404" />
		</a>
	</div>
</body>
</html>