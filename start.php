<?php
include("./include/public/conf/include.php");
include("./include/public/conf/public_conf.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta name="Keywords" content="<?php echo $KEYWORDS; ?>">
	<meta name="Description" content="<?php echo $DESCRIPTION; ?>">
	<meta name="robots" content="all">
	<title>
		<?php
		echo $TITLE;
		?>
	</title>
	
	<link rel="shortcut icon" href="./image/icon.ico">
	
	<link rel="stylesheet" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 
	<link rel="stylesheet" href="./css/public.css">
	
	<script type="text/javascript" src="./fancybox/jquery-1.4.3.js"></script>
	<script type="text/javascript" src="./fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="./js/main.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {	
			$('.image_zoom').fancybox();
			
			$('a[rel=diaporama_group]').fancybox({
				'transitionIn'		: 'elastic',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});
		});
	</script>
</head>

<body onload="ShowStatus()">
	<div id="public_include">
		<div id="banniere">
			<?php
			include('./include/public/main/public_top.php');
			?>
		</div>
		<div id="menu">
			<?php
			include('./include/public/main/public_menu.php');
			?>
		</div>
		<div id="main">
			<?php
			include('./include/public/main/public_body.php');
			?>
		</div>
	</div>
</body>
</html>