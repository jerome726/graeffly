<?php
include("./include/public/conf/include.php");
include("./include/public/conf/public_conf.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta name="Keywords" content="<?php echo $KEYWORDS; ?>">
	<meta name="Description" content="<?php echo $DESCRIPTION; ?>">
    <meta name="robots" content="all">
	<title>
		<?php
		echo $TITLE;
		?>
	</title>
	
	<link rel="shortcut icon" href="./image/icon.ico">
	
	<link rel="stylesheet" type="text/css" href="./css/index.css">
	<script type="text/javascript" src="./js/main.js">
	</script>
</head>

<body onload="ShowStatus()">
<?php
	// On enregsitre l'ip
	$stat = new Stat();
	$stat->insertBddStat();
?>
	<div id="enter">
		<a href="start.php?num_page=1">
			<img src="./image/logo.jpg">
		</a>
	</div>
</body>
</html>